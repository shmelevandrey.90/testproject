# Локальная сборка проекта

## Подготовка

- установить docker
- в качестве кластера рекомендуется установить [minikube](https://minikube.sigs.k8s.io/docs/start/). Так же можно
  использовать [Docker desktop](https://docs.docker.com/desktop/kubernetes/)
- установить [Helm](https://helm.sh/docs/intro/install/)
- установить [Taskfile](https://taskfile.dev/installation/)
- установить [k9s](https://k9scli.io/topics/install/)
- не установливать!!! [skaffold](https://skaffold.dev/docs/install/)
